<!DOCTYPE html>
<html lang="en">
  <head>
	<meta charset="utf-8">
	  <title> Sign Up </title>
  </head>

  <body>
  	<h1> Buat Account Baru! </h1>
	  <h3> Sign Up Form </h3>
	  

	<form action="/welcome" method="post">
        @csrf
		<label>First Name:</label> <br> <br>
		<input type="text" name="fname"> <br> <br>
		<label for="lname">Last Name:</label> <br> <br>
		<input type="text" name="lname"> <br> <br>
		<!--<label for="tempat_lahir">Tempat Lahir</label> <br> <br>
		<input type="text" id="tempat_lahir" name="tempatlahir"> <br> <br>
		<label for="tanggal_lahir">Tanggal Lahir</label> <br> <br>
    <input type="date" id="tanggal_lahir" name="birthday"> <br> <br>-->
    <label>Gender:</label> <br> <br>
		<input type="radio" id="male" name="gender" value="Male">
    <label for="male">Male</label> <br>
    <input type="radio" id="female" name="gender" value="Female">
    <label for="female">Female</label> <br>
    <input type="radio" id="other" name="gender" value="Other">
    <label for="other">Other</label> <br> <br>
		<label>Nationality:</label> <br> <br>
		<select name="nat">
			<option value="Indonesia">Indonesia</option>
			<option value="Amerika">Amerika</option>
			<option value="Inggris">Inggris</option>
		</select> <br> <br>
		<label>Language spoken</label> <br> <br>
		<input type="checkbox" id="ind" name="fav_language1" value="Bahasa Indonesia">
    <label for="ind">Bahasa Indonesia</label> <br>
    <input type="checkbox" id="eng" name="fav_language2" value="English">
    <label for="eng">English</label> <br>
    <input type="checkbox" id="others" name="fav_language3" value="Other">
    <label for="others">Other</label> <br> <br>
    <!--<label>Divisi/Departemen</label> <br>
		<input type="radio" id="deptkeu" name="dept" value="Dept Keu">
    <label for="deptkeu">Departemen Keuangan</label> <br>
    <input type="radio" id="divlap" name="dept" value="Div Lap">
    <label for="divlap">Divisi Lapangan</label> <br>
    <input type="radio" id="deptprs" name="dept" value="Dept Personlia">
    <label for="deptprs">Departemen Personalia</label> <br> <br>-->
		<label>Bio</label> <br> <br>
		<textarea name="bio" cols="30" rows="10"></textarea> <br> <br>

		<input type="submit" value="Sign Up">


  </body>
</html>